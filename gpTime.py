#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 获取当前脚本文件所在目录的父目录，并构建相对路径
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
project_path = os.path.join(current_dir, '..')
sys.path.append(project_path)
sys.path.append(current_dir)
import gpLog
import paho.mqtt.client as mqtt
from datetime import datetime

def getCurrentDateTimeStamp():
    ret = datetime.now().timestamp()
    ret = int(ret)
    return ret

def getDateTimeFromStamp(timestamp):
    date_time = datetime.utcfromtimestamp(timestamp)

def getStrFromTimeStamp(timestamp):
    dt = getDateTimeFromStamp(timestamp)
    formatted_date_time = dt.strftime("%Y-%m-%d %H:%M:%S")
    return formatted_date_time

def getCurrentDateTimeStr():
    # 获取当前时间
    now = datetime.now()
    # 按照月、日、时、分、毫秒的格式打印
    formatted_time = now.strftime("%m-%d %H:%M:%f")[:-3]
    return formatted_time
