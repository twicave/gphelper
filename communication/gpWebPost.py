#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 获取当前脚本文件所在目录的父目录，并构建相对路径
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
project_path = os.path.join(current_dir, '..')
sys.path.append(project_path)
sys.path.append(current_dir)
import gpLog
import paho.mqtt.client as mqtt
from datetime import datetime
import cv2
import requests
import numpy as np

def gpWebPost_PostImage(url:str, imgFilenameInFiles, cv2Image, ai_classNameInData, ai_class:int):
    if not url.startswith("http://"):
        # 设置 POST 请求的 URL
        url = f'http://{url}'

    # 设置要发送的文件
    files = {
        f'{imgFilenameInFiles}': cv2Image  # 将文件打开为二进制模式
    }

    data = {
    f'{ai_classNameInData}': ai_class,
    }

    jsonArg ={}
    jsonArg[f"{ai_classNameInData}"]=ai_class
    # 发送 POST 请求
    response = requests.post(url, files=files, json=jsonArg, data = data)

    # 检查响应
    if response.status_code == 200:
        return True
    else:
        return False

def gpWebPost_TestPostImage():
    # 读取图像
    url = "http://192.168.0.1:5000/sys/img/ai/uploadImg/camera01/1"
    image = cv2.imread(r'./data/captured_image.jpg')
    

    # 将图像转换为字节流
    _, img_encoded = cv2.imencode('.jpg', image)
    image_data = img_encoded.tobytes()
    gpWebPost_PostImage(url, "snapshot.jpg", image_data, "detect_classes", 1)

