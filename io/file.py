#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 获取当前脚本文件所在目录的父目录，并构建相对路径
# usage of this file from another project:
'''
sys.path.append('../gphelper/io')  #use related dir.
from file import gpfiles #import this file.

    gpfiles.makesure_path_exists("/root/ggg")
'''
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
project_path = os.path.join(current_dir, '..')
sys.path.append(project_path)
sys.path.append(current_dir)
import gpLog
from datetime import datetime
import io
import os

class gpfiles:
    #return yolo11s
    @staticmethod
    def get_filename_without_ext(path=r"/app/rk3588_build/yolo_sdk/ultralytics/yolo11s.pt"):
        # 获取最后一个文件名
        filename = os.path.basename(path)
        # 去除文件扩展名
        name_without_ext = os.path.splitext(filename)[0]
        # 提取所需部分
        result = name_without_ext[:-1] if name_without_ext.endswith('s') else name_without_ext

        print(result)

    @staticmethod
    def makesure_path_exists(path):
        # 定义要创建的目录路径
        directory = 'path/to/your/directory'

        # 检查目录是否存在，如果不存在则创建
        if not os.path.exists(directory):
            os.makedirs(directory)
            print(f"目录 {directory} 创建成功")
        else:
            print(f"目录 {directory} 已经存在")


    @staticmethod
    def delete_txt_files(directory, post_str = '.txt'):
        # 检查目录是否存在
        if not os.path.exists(directory):
            print(f"指定的目录 {directory} 不存在。")
            return
        # 遍历目录下的所有文件和文件夹
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(post_str):
                    file_path = os.path.join(root, file)
                    try:
                        # 删除文件
                        os.remove(file_path)
                        print(f"已删除文件: {file_path}")
                    except Exception as e:
                        print(f"删除文件 {file_path} 时出错: {e}")

    @staticmethod
    def delete_files_by_ext(directory, post_str = '.txt'):
        # 检查目录是否存在
        if not os.path.exists(directory):
            print(f"指定的目录 {directory} 不存在。")
            return
        # 遍历目录下的所有文件和文件夹
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(post_str):
                    file_path = os.path.join(root, file)
                    try:
                        # 删除文件
                        os.remove(file_path)
                        print(f"已删除文件: {file_path}")
                    except Exception as e:
                        print(f"删除文件 {file_path} 时出错: {e}")
