#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 获取当前脚本文件所在目录的父目录，并构建相对路径
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
project_path = os.path.join(current_dir, '..')
sys.path.append(project_path)
sys.path.append(current_dir)
import datetime
import json

class GpCfg:
    def __init__(self, json_filename):
        self.filename = json_filename;
        self.cfg = self.load_json_cfg()

    def load_json_cfg(self):
        if(self.filename[0]=='/') or (self.filename[0]=='\\'):
            fullpathname = self.filename
        else:
            fullpathname = os.path.join(project_path, self.filename)

        # 读取JSON文件
        print('++++++',fullpathname)
        with open(fullpathname, 'r', encoding='utf-8') as file:
            cfg = json.load(file)
        return cfg