#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 获取当前脚本文件所在目录的父目录，并构建相对路径
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
project_path = os.path.join(current_dir, '..')
sys.path.append(project_path)
sys.path.append(current_dir)

import logging
from logging.handlers import RotatingFileHandler
import datetime

logger = None;
def logInit(logName, logfilename):
	global logger
	# 设置日志级别为DEBUG
	logging.basicConfig(level=logging.INFO)

	# 创建日志记录器
	logger = logging.getLogger(logName)
	logger.setLevel(logging.INFO)
	
	# 创建一个RotatingFileHandler，实现日志文件的滚动和控制文件大小
	handler = RotatingFileHandler(logfilename, maxBytes=10000, backupCount=3)
	handler.setLevel(logging.INFO)

	# 创建日志记录格式
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	handler.setFormatter(formatter)
	
	# 添加StreamHandler到日志记录器
	logger.addHandler(handler)

def gLog():
	global logger
	if(logger is None):
		logInit("daemon_mqtt_message.log");
	return logger
	
def gLog_example():
	# 记录日志信息
	logger.debug("This is a debug message")
	logger.info("This is an info message")
	logger.warning("This is a warning message")
	logger.error("This is an error message")
	logger.critical("This is a critical message")