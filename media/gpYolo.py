#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 获取当前脚本文件所在目录的父目录，并构建相对路径
# usage of this file from another project:
'''
sys.path.append('../gphelper/io')  #use related dir.
from file import gpfiles #import this file.

    gpfiles.makesure_path_exists("/root/ggg")
'''
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
project_path = os.path.join(current_dir, '..')
sys.path.append(project_path)
sys.path.append(current_dir)
import gpLog
from datetime import datetime
import cv2

class gpYolo:
    #return yolo11s
    @staticmethod
    def frame_resize_before_detect(frame):
        # 调整帧大小为 640x640
        resized_frame = cv2.resize(frame, (640, 640))
        return resized_frame
    
    @staticmethod
    def frame_resize_after_detect(frame, width, height):
        # 调整帧大小为 640x640
        resized_frame = cv2.resize(frame, (width, height))
        return resized_frame

    @staticmethod
    def yolo_detected_box_2_real_framebox(x1, y1, x2, y2, width, height):
        # 计算缩放比例
        scale_x = width / 640
        scale_y = height / 640
        # 还原坐标
        x1 = int(x1 * scale_x)
        y1 = int(y1 * scale_y)
        x2 = int(x2 * scale_x)
        y2 = int(y2 * scale_y)
        return x1, y1, x2, y2