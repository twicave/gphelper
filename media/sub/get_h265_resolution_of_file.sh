#!/bin/bash
# 获取脚本所在的目录
SCRIPT_DIR=$(dirname "$(realpath "$0")")
# 切换到脚本所在的目录
cd "$SCRIPT_DIR" || exit
file_of_h265=$1

# 运行程序并捕获输出
output=$(python3 ./fetch_from_stream.py "$file_of_h265")
# 解析输出结果，假设输出格式为 "1920,1080"
IFS=',' read -r width height <<< "$output"
mpi_enc_test -i frame_by_soft_decode_of_h265file.bin -w $width -h $height -t 16777220 -o 01.h265 -n 20
./get_h265_resolution ./01.h265
