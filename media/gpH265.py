#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ��ȡ��ǰ�ű��ļ�����Ŀ¼�ĸ�Ŀ¼�����������·��
import os
import sys
current_dir = os.path.dirname(os.path.abspath(__file__))
project_path = os.path.join(current_dir, '..')
sys.path.append(project_path)
sys.path.append(current_dir)
import subprocess

def gp_h265_get_real_solution_of_file(file_path):
    # ���� .sh �ű�����ȡ���
    result = subprocess.run(['bash', os.path.join(current_dir, 'sub/get_h265_resolution_of_file.sh'), file_path], capture_output=True, text=True)
    
    # ��ȡ��׼���
    output = result.stdout.strip()
    
    print(output)
    # �ָ�����ַ�������ת��Ϊ����
    numbers = [int(num) for num in output.split(',')]
    return numbers

def gp_h265_get_real_solution_of_stream(rtsp_path):
    # ���� .sh �ű�����ȡ���
    result = subprocess.run(['bash', os.path.join(current_dir, 'sub/get_h265_resolution_of_stream.sh'), rtsp_path], capture_output=True, text=True)
    print(result)
    
    # ��ȡ��׼���
    output = result.stdout.strip()
    
    # �ָ�����ַ�������ת��Ϊ����
    numbers = [int(num) for num in output.split(',')]
    return numbers


#usage:
if __name__ == "__main__":
    file_path = "./Tennis1080p.h265"
    print(gp_h265_get_real_solution_of_file(file_path))
    rtsp_path = "rtsp://admin:xxxx@192.168.0.1:554/h265/ch1/main/av_stream"
    print(gp_h265_get_real_solution_of_stream(file_path))
